# Solution guide

The solution works with ASCII characters. The valid characters are the lowercase or uppercase alphabets. <br>
'#' - is a control character that exits the app.<br>
The simplified console structure is used intentionally to avoid overcomplicating the solution.
All solution classes inherit from and implement interfaces, making them perfect targets for Dependency Injection (DI)
to inject desired behavior depending on the intentions.

### InputValidator

It consumes user character input, validates, and converts the entered character.

### DynamicDiamond

It consumes no memory. It returns a character based on a calculated index.
There are two iterators in addition to the indexer implemented in the class itself.

#### Iterators

Both iterators under the hood use DynamicDiamond's indexer.

__Enumerable__ - Implements IEnumerable<> and IEnumerator<> interfaces
to enable iteration through the Diamond's characters. It iterates row by row,
consequently returning the row's and column's values, incrementing or resetting them
if the column's end is reached.

__Yielded__ - yields the cell's dynamic value calculated in the file by the DynamicDiamond indexer.

### Output

An output strategy that is built on top of the diamond's base part. The dynamic base part is the matrix containing elements of the
top left part of the diamond when split into four parts:

diamond base:
```
--a
-b-
c--
```

if diamond is:
```
--a--
-b-b-
c---c
-b-b-
--a--
```

Implemented a couple of output strategies:

* __In-memory diamond output__ populates an in-memory matrix with a similar structure to the example above.
* __Console output__ prints the diamond to the console.

Both outputs minimize the memory footprint until the diamond data is requested.

__TopLeftDiamondPartBuilder__ constructs the diamond base using the provided character and base code.

__Diamond character__: This character defines the point of symmetry.
__Base code__: This is the ASCII code of a character that comes right before the first lowercase or uppercase alphabet:
* 64 for uppercase
* 96 for lowercase

### Designed Patterns used

* __Builder__ - Constructs the top-left part of the diamond.
* __Strategy__ - Outputs the diamond either to the console or to memory.
* __Iterator__ - Calculates the diamond cell's value on the fly with minimal memory footprint.

### Design Principles used

* __Composition__
* __Aggregation__
* __Interface Segregation__
* __Single Responsibility__

