# Payment system

__Assumptions__:
* ~5 RPS (Requests Per-Second)
* Pay-in and Pay-out flows
* Leveraging PSP (Payment Service Provider) and Payout service Provider
* Support one currency that is US dollar ($)
* Reliable
* Fault tolerant
* Consistent

Functional requirements:
* Pay-in - customer pays for the requested good flow
* Pay-out - merchant receives payments for the sold goods

Non-functional requirement:
* reliability
* resiliency
* fault tolerance
* consistency
are the key.

## Pay-in

### High level diagram:

![High Level Diagram](./Image/PaymentSystemHighLevelDiagram.png)

List of services:
- __[PRS - Payment Request Service]__ - receives payment request, stores it in the persistent storage, orchestrates feather processing steps
- __[LS - Ledger Service]__ - manages orders record history
- __[WS - Wallet Service]__ - holds the merchant's account balance
- __[POS _ Payment Order Service]__ - orchestrates payments requests to the PSP (Payment Service Provider)
- __[PSP - Payment Service Provider]__ - a 3rd party services authorized to facilitate actual money transfers from client's bank account to the custodial bank account

Typical Flow(s):

__Happy Path__

1. Payment Request Service receives the payment request
2. PRS stores the received request in persistent storage
3. PRS sends requests to the Payment Order Service; if multiple items purchased - PRS splits purchase request to the multiple orders and send each order to the POS
4. POS stores order(s) to persistent storage
5. POS sends the order request to the PSP
6. PSP confirms payment and redirects user to the "Successful Payment" page ___[Back to the merchant link]___
7. POS confirms the order execution, updates order status
8. PRS marks the payment request as successfully executed 
   * sends update to Wallet Service (WS)
   * sends update to Ledger Service (LS)

__Payment Canceled__

6. PSP cancels payment and redirects the user to the "Payment was canceled" page ___[Back to the merchant link]___
7. POS marks the order as canceled
8. PRS marks the request or the corresponding part of the request as canceled, if there were multiple items, as canceled

__Payment Failed__

6. PSP fails to process the payment request, it redirects the user to the "Payment failed" page ___[Back to the customer link]___
7. POS marks the order as failed __!It is important to mention that certain failures can be retried, but not all of them__
8. PRS marks the request or part of the request as failed

__!! Important notes !!__
* All communication between the POS and PSP must be encrypted. Typically, the PSP provide public and private keys POS uses to sign the requests sent to the PSP. Then PSP then validates the requests using the same key-pair.
The same applies to web hook notifications. To verify the authenticity of notification, the POS can request a public key from the PSP for verification. The PSPs key for the web hook process can rotate.
To requested it POS send the signed request to the PSP.
* Often there is no guarantee in the web hook notification ordering, messages can be delayed, stacked, lost.
* In the diagram, the database (DB) is depicted as a single element. However, this does not imply a single DB for all services. It's crucial to carefully choose the storage that fits each service's requirements. 
___For instance,___ _for some cases, a suitable option might be a key-value storage (e.g., AWS DynamoDB) with a few additional required attributes and references to the remaining persisted data stored in blob storage._
* Industry standard dictates to use time proven, relational DB but it is a subject for debates due to modern NoSQL dbs are time proven and can reduce the cost and DevOps churn.
* f a relational DB is used, it's vital to be cautious with foreign keys. Unnecessary dependencies can reduce query performance and the overall responsiveness of the system. It's acceptable to keep the data relationship model simple, 
or even have no relationships at all, opting for flat denormalized data tables. For analytics and complex queries, consider using a separate storage solution or building an ETL (Extract, Transform, Load) process. 

### Checkout Process

![Checkout Process Diagram](./Image/CheckoutProcessDiagram.png)

Typical Flow(s):

1. The user clicks the "Purchase" or "Checkout" button.
2. The Payment System (PS) collects all necessary information: currency, amount to be charged, request expiration time, a unique payment ID (often referred to as a nonce), and redirect URLs. The user is then redirected to the PSP's payment processing widget or page. Some PSPs provide Client SDKs for integration.
3. The PSP responds with a registered payment ID. This ID is used and returned as one of the parameters in subsequent communication.
4. The Payment System (PS) stores this ID and uses it as a reference for the corresponding internal data in the event of a change in payment request status.
5. The PSP asynchronously sends the payment status back through callbacks or web hooks. Additionally, some PSPs might send payment details as parameters through redirects to the merchant's page, including success, cancel, and fail requests.

### Double-entry ledger


It's crucial to uphold a significant accounting principle of maintaining two distinct ledger accounts: the debit and credit accounts. In this context, the buyer's account is debited, while the seller's account is credited. 
The total sum of these two accounts must equate to zero. If this balance is not zero, it indicates a discrepancy, and the system requires reconciliation to identify and address any discrepancies or inconsistencies.

### Reconciliation

The PS and PSP communicate asynchronously, implying that there is no guarantee of message delivery or the invocation of the callback endpoint. These requests share a common identifier like all other requests.

To ensure the consistency of all order transactions within the PS, reconciliation needs to occur on a scheduled basis. Typically, the PSP sends a Settlement file that encompasses a record of all transactions. This file serves the purpose of identifying any discrepancies within the PS system. Three types of mismatches exist:

1. __Classified mismatch that can be auto-resolved:__ These discrepancies are categorized and can be resolved through automated adjustments. Software engineers can devise a program to handle these adjustments.
2. __Classified mismatch that requires manual intervention:__ Certain mismatches fall into this category, demanding resolution that surpasses automated methods.
3. __Unclassified mismatch:__ This type of mismatch necessitates in-depth manual investigation to uncover the underlying root cause.

### Inter-service communication

Internally, the services of the PS can either utilize a synchronous protocol for communication, such as HTTP, or engage in asynchronous communication through a message queue. 

However, synchronous communication carries certain drawbacks:
* __Low performance:__ Chained requests tend to exhibit suboptimal performance, especially for the service that resides at the end of the chain.
* __Cascade failures and lack of failure isolation:__ If any service in the chain experiences a failure, it has a cascading effect, causing the entire chain to fail. This lack of isolation magnifies the impact of failures.
* __Tight coupling:__ Services become tightly coupled due to their dependence on knowing the address of their dependencies, which can hinder flexibility and agility.
* __Difficulty in scaling:__ Scaling synchronous services can be complex, and at times, challenging to achieve effectively.
It's important to consider these drawbacks when selecting a communication approach for the services within the PS.

Asynchronous communication is a superior choice for complex systems. While it isn't a one-size-fits-all solution, it offers several advantages:

* __Improved failure isolation:__ Asynchronous communication enhances the isolation of failures. If one component encounters an issue, it's less likely to propagate throughout the entire system.
* __Enhanced service independence:__ Components can operate with greater independence in an asynchronous system, reducing tight coupling and allowing for more modular and maintainable architecture.
* __Support for diverse delivery targets:__ Asynchronous communication can accommodate both single and multiple delivery targets, making it adaptable to various use cases and integration scenarios.
* __Flexible scalability:__ This approach facilitates more flexible scalability by decoupling components, enabling them to scale independently and effectively handle varying workloads.
By leveraging these benefits, asynchronous communication can contribute to the efficiency and robustness of complex systems.

### Failures and Retries

Every system fails. PS need to be resilient and fault tolerant, no one wants loose money. When failure happens PS have to handle it in a predictable manner.
With message queue use:
* failed request message queue - contains request that are failed to execute but can be retried, for example transient error
* dead-letter queue - if message keep failing put it into the dead letter queue

Avoid to frequent request that can overwhelm underling services, for that use:
* user threshold - defines the maximum number of retries
* exponential backoff - with every failed retry double the next retry timeout
* it is a good practice use retry headers if sending request to the 3rd parti service, a lot of services provide the next retry timeout if the system that has been called is overwhelmed 
* rate limits and circuit breakers - used to automatically reject request in case of the underling system is overwhelmed or the client reached "requests per timeframe" threshold 

Every system is susceptible to failures. The PS must exhibit resilience and fault tolerance to ensure financial losses are avoided. In the event of a failure, the PS should manage it in a predictable manner.

When employing a message queue, the following strategies are recommended:

* __Failed Request Message Queue:__ This queue holds requests that have failed to execute but are candidates for retry, such as those stemming from transient errors.
* __Dead-Letter Queue:__ If a message repeatedly fails, it should be redirected to the dead-letter queue.

To prevent overwhelming underlying services, consider the following approaches:

* __User Threshold:__ Define the maximum allowable number of retries per request.
* __Exponential Backoff:__ For each failed retry attempt, exponentially increase the subsequent retry timeout.
* __Retry Headers:__ When sending requests to third-party services, employ retry headers. Many services furnish information about the next retry timeout if their system is overwhelmed.
* __Rate Limits and Circuit Breakers:__ Implement rate limits and circuit breakers to automatically reject requests when the underlying system is overwhelmed or a client surpasses a "requests per timeframe" threshold.

By adhering to these practices, the PS can manage failures effectively and ensure the stability of its operations.

__Idempotency__

Idempotency is a critical attribute of the PS that ensures the preservation of data consistency. For instance, if a user clicks the "Checkout" button multiple times due to extended request times or failures, 
the system must possess the capability to discern duplicate requests and respond accordingly. This involves providing the user with updates on the progress of the initial request.

Each Checkout request within the PS is equipped with a unique ID that serves as a reference for the underlying payment process. Conversely, the PSP issues a distinct token for every fresh payment request. 
On the side of the POS, a nonce parameter is employed to differentiate between user purchases. Furthermore, this parameter facilitates the connection of PS data to the PSP requests.

(__Idempotency__ - is a system behavior in which sending the same request to the system multiple times does not initiate new execution flows but instead offers the status of the initial request.)

## Pay-out

The Pay-out process follows the same design as the Pay-in flow, differing only in the substitution of the PSP with the service responsible for delivering Pay-out functionality.