﻿using DiamondKata.Builder;
using DiamondKata.DynamicDiamond;
using DiamondKata.DynamicDiamond.Iterator;
using DiamondKata.Output;
using DiamondKata.Printer;
using DiamondKata.Validator;

while (true)
{
    Console.WriteLine("Please enter a Key. It should be an valid Alphabet symbol or # to exit app");
    var center = Console.ReadKey();
    if (!center.KeyChar.IfDiamondKataSymbol(out byte code, out byte baseCode))
    {
        if (center.KeyChar == '#')
        {
            return;
        }
        
        Console.WriteLine();
        Console.WriteLine("Wrong key value! Key must be a valid alphabet symbol!");
        continue;
    }

    var diamondBase = new TopLeftDiamondPartBuilder().Build(code, baseCode);
    Console.WriteLine(); Console.WriteLine("The Diamond Base:");
    new TopLeftDiamondPartPrinter(diamondBase, new ConsoleOutput(diamondBase)).Print();
    
    // Console.WriteLine();
    // Console.WriteLine("The Diamond Top Half:");
    // new TopDiamondHaltPrinter(diamondBase, new ConsoleOutput(diamondBase)).Print();
    // 
    // Console.WriteLine();
    // Console.WriteLine("The Diamond Bottom Left:");
    // new BottomLeftDiamondPartPrinter(diamondBase, new ConsoleOutput(diamondBase)).Print();
    // 
    // Console.WriteLine();
    // Console.WriteLine("The Diamond Bottom Half:");
    // new BottomDiamondHalfPrinter(diamondBase, new ConsoleOutput(diamondBase)).Print();
     
    Console.WriteLine();
    Console.WriteLine("The Diamond:");
    new DiamondPrinter(diamondBase, new ConsoleOutput(diamondBase)).Print();
    
    Console.WriteLine();
    Console.WriteLine("The Diamond from Memory structure:");
    var output = new MemoryOutput(diamondBase);
    new DiamondPrinter(diamondBase, output).Print();
    for (var i = 0; i < output.DiamondDimension; i++)
    {
        for (var j = 0; j < output.DiamondDimension; j++)
        {
            Console.Write(output.Diamond[i, j]);
        }
        Console.Write(Environment.NewLine);
    }
    
    Console.WriteLine();
    Console.WriteLine("Iterate through The Diamond from Memory structure:");
    for (var i = 0; i < output.DiamondDimension; i++)
    {
        for (var j = 0; j < output.DiamondDimension; j++)
        {
            Console.Write(output[i, j]);
        }
        Console.Write(Environment.NewLine);
    }
    
    Console.WriteLine();
    Console.WriteLine("Iterate through The Dynamic Diamond:");
    var dynamicDiamond = new DynamicDiamond(code, baseCode);
    for (var i = 0; i < dynamicDiamond.DiamondDimension; i++)
    {
        for (var j = 0; j < dynamicDiamond.DiamondDimension; j++)
        {
            Console.Write(dynamicDiamond[i, j]);
        }
        Console.Write(Environment.NewLine);
    }
    
    Console.WriteLine();
    Console.WriteLine("Iterate through The Dynamic Diamond via Foreach:");
    var enumerableDiamond = new EnumerableDiamond(code, baseCode);
    foreach (var cell in enumerableDiamond)
    {
        Console.Write(cell);
        if (enumerableDiamond.Column == enumerableDiamond.DiamondDimension - 1)
        {
            Console.Write(Environment.NewLine);
        }
    }
    
    Console.WriteLine();
    Console.WriteLine("Iterate through The Dynamic Diamond via Foreach from yielded function:");
    var yieldedDiamond = new YieldedDiamond(code, baseCode);
    foreach (var cell in yieldedDiamond.Diamond())
    {
        Console.Write(cell);
        if (yieldedDiamond.Column == yieldedDiamond.DiamondDimension - 1)
        {
            Console.Write(Environment.NewLine);
        }
    }
    Console.WriteLine();
}