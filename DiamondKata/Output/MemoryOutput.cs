using DiamondKata.Enum;

namespace DiamondKata.Output;

public class MemoryOutput: IOutput, IMemoryOutput
{
    private const char EmptyCell = ' ';
    private readonly char[,] _diamondBase;
    public int DiamondDimension { get; private set; }
    
    private DiamondPart _currentDiamondPartToOutput = DiamondPart.TopLeft;
    public char[,] Diamond { get; private set; }
    
    public MemoryOutput(char[,] diamondBase)
    {
        _diamondBase = diamondBase;
        DiamondDimension = diamondBase.GetLength(0) * 2 - 1;
        Diamond = new char[DiamondDimension, DiamondDimension];
        for (var i = 0; i < DiamondDimension; i++)
        {
            for (var j = 0; j < DiamondDimension; j++)
            {
                Diamond[i, j] = EmptyCell;
            }
        }
    }
    
    public void Put(int i, int j)
    {
        switch (_currentDiamondPartToOutput)
        {
            case DiamondPart.TopLeft:
                Diamond[i, j] = _diamondBase[i, j];
                break;
            case DiamondPart.TopRight:
                Diamond[i, DiamondDimension - j - 1] = _diamondBase[i, j];
                break;
            case DiamondPart.BottomLeft:
                Diamond[DiamondDimension - i - 1, j] = _diamondBase[i, j];
                break;
            case DiamondPart.BottomRight:
                Diamond[DiamondDimension - i - 1, DiamondDimension - j - 1] = _diamondBase[i, j];
                break;
        }
    }

    public void SetPrintingPart(DiamondPart printingPart)
    {
        _currentDiamondPartToOutput = printingPart;
    }

    public char this[int row, int column]
    {
        get
        {
            var diamondPartByPosition = GetDiamondPartByPosition(row, column);
            var diamondBaseDimension = _diamondBase.GetLength(0);
            switch (diamondPartByPosition)
            {
                case DiamondPart.TopLeft:
                    return _diamondBase[row, column];
                case DiamondPart.TopRight:
                    return _diamondBase[row, 2 * diamondBaseDimension - column - 2];
                case DiamondPart.BottomLeft:
                    return _diamondBase[2 * diamondBaseDimension - row - 2, column];
                case DiamondPart.BottomRight:
                    return _diamondBase[2 * diamondBaseDimension - row - 2, 2 * diamondBaseDimension - column - 2];
            }

            throw new IndexOutOfRangeException();
        }
    }

    private DiamondPart GetDiamondPartByPosition(int row, int column)
    {
        var diamondBaseDimension = _diamondBase.GetLength(0);
        if (row < diamondBaseDimension && column < diamondBaseDimension)
        {
            return DiamondPart.TopLeft;
        } 
        else if (row < diamondBaseDimension && column >= diamondBaseDimension)
        {
            return DiamondPart.TopRight;
        }
        else if (row >= diamondBaseDimension && column < diamondBaseDimension)
        {
            return DiamondPart.BottomLeft;
        }
        else if (row >= diamondBaseDimension && column >= diamondBaseDimension)
        {
            return DiamondPart.BottomRight;
        }

        throw new IndexOutOfRangeException();
    }
}