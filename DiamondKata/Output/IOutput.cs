namespace DiamondKata.Output;

public interface IOutput
{
    public void Put(int i, int j);
}