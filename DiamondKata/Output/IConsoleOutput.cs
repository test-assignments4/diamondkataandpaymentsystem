namespace DiamondKata.Output;

public interface IConsoleOutput
{
    public void NewLine();
}