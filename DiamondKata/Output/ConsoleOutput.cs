namespace DiamondKata.Output;

public class ConsoleOutput: IOutput, IConsoleOutput
{
    private readonly char[,] _diamondBase;
    public ConsoleOutput(char[,] diamondBase)
    {
        _diamondBase = diamondBase;
    }
    
    public void Put(int i, int j)
    {
        Console.Write($"{_diamondBase[i, j]}");
    }

    public void NewLine()
    {
        Console.Write(Environment.NewLine);
    }
}