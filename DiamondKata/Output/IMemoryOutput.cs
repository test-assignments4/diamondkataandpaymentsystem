using System.ComponentModel.DataAnnotations.Schema;
using DiamondKata.Enum;

namespace DiamondKata.Output;

public interface IMemoryOutput
{
   public char[,] Diamond { get; }
   
   public char this[int row, int column] 
   {
      get;
   }
   
   public int DiamondDimension { get; }
   public void SetPrintingPart(DiamondPart printingPart);
}