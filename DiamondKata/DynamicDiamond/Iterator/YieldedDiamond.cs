namespace DiamondKata.DynamicDiamond.Iterator;

public class YieldedDiamond
{
    private readonly DynamicDiamond _dynamicDiamond;
    
    public int Column { get; private set; } 
    public int Row { get; private set; }
    
    public int DiamondDimension => _dynamicDiamond.DiamondDimension;
    
    public YieldedDiamond(byte code, byte baseCode)
    {
        _dynamicDiamond = new DynamicDiamond(code, baseCode);
    }
    
    public IEnumerable<char> Diamond()
    {
        for(var row = 0; row < DiamondDimension; row++)
        {
            Row = row;
            for (var column = 0; column < DiamondDimension; column++)
            {
                Column = column;
                yield return _dynamicDiamond[row, column];
            }
        }
    }
}