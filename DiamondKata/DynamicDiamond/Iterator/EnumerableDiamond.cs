using System.Collections;

namespace DiamondKata.DynamicDiamond.Iterator;

public class EnumerableDiamond : IEnumerable<char>, IEnumerator<char>
{
    private int _row = 0;
    private int _column = -1;
    private readonly DynamicDiamond _dynamicDiamond;
    
    private char _Current => _dynamicDiamond[_row, _column];
    
    public int Column => _column;
    public int Row => _row;

    public int DiamondDimension => _dynamicDiamond.DiamondDimension;

    public EnumerableDiamond(byte code, byte baseCode)
    {
        _dynamicDiamond = new DynamicDiamond(code, baseCode);
    }
    
    public bool MoveNext()
    {
        if (_column == DiamondDimension - 1 && _row != DiamondDimension - 1)
        {
            _row += 1;
            _column = 0;
            return true;
        }
        else if (_column == DiamondDimension - 1 && _row == DiamondDimension - 1)
        {
            return false;
        }
        else
        {
            _column += 1;
            return true;
        }
    }

    public char Current => _Current;

    public void Reset()
    {
        _row = 0;
        _column = -1;
    }

    object IEnumerator.Current => _Current;

    IEnumerator IEnumerable.GetEnumerator()
    {
        return (IEnumerator)this;
    }

    IEnumerator<char> IEnumerable<char>.GetEnumerator()
    {
        return this;
    }

    public void Dispose()
    {
    }
}