using DiamondKata.Enum;

namespace DiamondKata.DynamicDiamond;

public class DynamicDiamond
{
    private const char EmptyCell = ' ';
    private readonly byte _code;
    private readonly byte _baseCode;
    private readonly int _diamondBaseDimension;
    
    public int DiamondDimension { get; private set; }
    
    public DynamicDiamond(byte code, byte baseCode)
    {
        _code = code;
        _baseCode = baseCode;
        _diamondBaseDimension = code - baseCode;
        DiamondDimension = 2 * _diamondBaseDimension - 1;
    }
    
    public char this[int row, int column]
    {
        get
        {
            var diamondPartByPosition = GetDiamondPartByPosition(row, column);
            switch (diamondPartByPosition)
            {
                case DiamondPart.TopLeft:
                    if (row + column != _diamondBaseDimension - 1)
                    {
                        return EmptyCell;
                    }
                    return (char)(_baseCode + _diamondBaseDimension - column);
                case DiamondPart.TopRight:
                    if (row != column - _diamondBaseDimension + 1)
                    {
                        return EmptyCell;
                    }
                    return (char)(_baseCode + row + 1);
                case DiamondPart.BottomLeft:
                    if (column != row - _diamondBaseDimension + 1)
                    {
                        return EmptyCell;
                    }
                    return (char)(_code - column);
                case DiamondPart.BottomRight:
                    var diagonal = column - _diamondBaseDimension + row - _diamondBaseDimension;
                    if (diagonal != _diamondBaseDimension - 3)
                    {
                        return EmptyCell;
                    }
                    return (char)(_code - _diamondBaseDimension + column - _diamondBaseDimension + 2);
            }
            throw new IndexOutOfRangeException();
        }
    }

    private DiamondPart GetDiamondPartByPosition(int row, int column)
    {
        var diamondBaseDimension = _code - _baseCode;
        if (row < diamondBaseDimension && column < diamondBaseDimension)
        {
            return DiamondPart.TopLeft;
        } 
        else if (row < diamondBaseDimension && column >= diamondBaseDimension)
        {
            return DiamondPart.TopRight;
        }
        else if (row >= diamondBaseDimension && column < diamondBaseDimension)
        {
            return DiamondPart.BottomLeft;
        }
        else if (row >= diamondBaseDimension && column >= diamondBaseDimension)
        {
            return DiamondPart.BottomRight;
        }

        throw new IndexOutOfRangeException();
    }
}