namespace DiamondKata.Validator;

public static class InputValidator
{
    private const byte LowerCaseAlphabetBase = 96;
    private const byte UpperCaseAlphabetBase = 64;
    
    private const byte LowerCaseAlphabetTop = 123;
    private const byte UpperCaseAlphabetTop = 91;
    
    public static bool IfDiamondKataSymbol(this char ch, out byte code, out byte baseCode)
    {
        var chCode = (byte)ch;
        if (chCode is > UpperCaseAlphabetBase and < UpperCaseAlphabetTop or > LowerCaseAlphabetBase and < LowerCaseAlphabetTop)
        {
            code = chCode;
            baseCode = chCode < 91 ? UpperCaseAlphabetBase : LowerCaseAlphabetBase;
            return true;
        }
        else
        {
            code = byte.MinValue;
            baseCode = byte.MinValue;
            return false;
        }
    }
}