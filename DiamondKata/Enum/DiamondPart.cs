namespace DiamondKata.Enum;

public enum DiamondPart
{
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight
}