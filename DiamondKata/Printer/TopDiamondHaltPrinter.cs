using DiamondKata.Output;

namespace DiamondKata.Printer;

public class TopDiamondHaltPrinter: IPrint
{
    private const char EmptyCell = ' ';
    private readonly char[,] _diamondBase;
    private readonly IOutput _output;
    public TopDiamondHaltPrinter(char[,] diamondBase, IOutput output)
    {
        _diamondBase = diamondBase;
        _output = output;
    }
    
    public void Print()
    {
        var dimension = _diamondBase.GetLength(0);
        if (dimension == 0)
        {
            _output.Put(0, 0);
            return;
        }

        var consoleOutput = _output as IConsoleOutput;
        for (var i = 0; i < dimension; i++)
        {
            var j = 0;
            while (j < dimension)
            {
                _output.Put(i, j);
                j++;
            }

            j -= 2;
            while(j >= 0)
            {
                _output.Put(i, j);
                if (_diamondBase[i, j] != EmptyCell)
                {
                    //does not print the spaces in the end of the line
                    break;
                }
                j--;
            }

            consoleOutput?.NewLine();
        }
    }
}