using DiamondKata.Output;

namespace DiamondKata.Printer;

public class BottomLeftDiamondPartPrinter: IPrint
{
    private readonly char[,] _diamondBase;
    private readonly IOutput _output;
    public BottomLeftDiamondPartPrinter(char[,] diamondBase, IOutput output)
    {
        _diamondBase = diamondBase;
        _output = output;
    }
    
    public void Print()
    {
        var dimension = _diamondBase.GetLength(0);
        if (dimension == 0)
        {
            _output.Put(0, 0);
            return;
        }
        
        var consoleOutput = _output as IConsoleOutput;
        for (var i = dimension - 1; i >= 0; i--)
        {
            for (var j = 0; j < dimension; j++)
            {
                _output.Put(i, j);
            }
            consoleOutput?.NewLine();
        }
    }
}