using DiamondKata.Enum;
using DiamondKata.Output;

namespace DiamondKata.Printer;

public class DiamondPrinter: IPrint
{
    private const char EmptyCell = ' ';
    private readonly char[,] _diamondBase;
    private readonly IOutput _output;
    public DiamondPrinter(char[,] diamondBase, IOutput output)
    {
        _diamondBase = diamondBase;
        _output = output;
    }
    
    public void Print()
    {
        
        var dimension = _diamondBase.GetLength(0);
        if (dimension == 0)
        {
            _output.Put(0, 0);
            return;
        }
        
        var consoleOutput = _output as IConsoleOutput;
        var memoryOutput = _output as IMemoryOutput;
        for (var i = 0; i < dimension; i++)
        {
            var j = 0;
            memoryOutput?.SetPrintingPart(DiamondPart.TopLeft);
            while (j < dimension)
            {
                _output.Put(i, j);
                j++;
            }

            j -= 2;
            memoryOutput?.SetPrintingPart(DiamondPart.TopRight);
            while(j >= 0)
            {
                _output.Put(i, j);
                if (_diamondBase[i, j] != EmptyCell)
                {
                    //does not print the spaces in the end of the line
                    break;
                }
                j--;
            }
            consoleOutput?.NewLine();
        }
        
        for (var i = dimension - 2; i >= 0; i--)
        {
            var j = 0;
            memoryOutput?.SetPrintingPart(DiamondPart.BottomLeft);
            while (j < dimension)
            {
                _output.Put(i, j);
                j++;
            }

            j -= 2;
            memoryOutput?.SetPrintingPart(DiamondPart.BottomRight);
            while(j >= 0)
            {
                _output.Put(i, j);
                if (_diamondBase[i, j] != EmptyCell)
                {
                    //does not print the spaces in the end of the line
                    break;
                }
                j--;
            }
            consoleOutput?.NewLine();
        }
    }
}