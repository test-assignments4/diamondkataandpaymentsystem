namespace DiamondKata.Builder;

public interface IBuilder
{
    public char[,] Build(byte code, byte baseCode);
}