﻿namespace DiamondKata.Builder;

public class TopLeftDiamondPartBuilder : IBuilder
{
    private const char EmptyCell = ' ';
    public char[,] Build(byte code, byte baseCode)
    {
        var dimension = code - baseCode;
        var leftTopPart = new char[dimension, dimension];
        for (var i = 0; i < dimension; i++)
        {
            for (var j = 0; j < dimension; j++)
            {
                leftTopPart[i, j] = EmptyCell;
            }
        }

        for (var d = 0; d < dimension; d++)
        {
            leftTopPart[dimension - d - 1, d] = (char)(code - d);
        }
        
        return leftTopPart;
    }
}