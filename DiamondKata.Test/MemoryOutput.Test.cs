using DiamondKata.Builder;
using DiamondKata.Output;
using DiamondKata.Printer;
using DiamondKata.Validator;

namespace DiamondKata.Test;

public class MemoryOutputTest
{
    private Dictionary<char, char[,]> lowCaseCharToDiamondMap;
    private Dictionary<char, char[,]> upCaseCharToDiamondMap;
    
    [SetUp]
    public void Setup()
    {
        lowCaseCharToDiamondMap = new Dictionary<char, char[,]>
        {
            {
                'a', 
                new char[,]
                {
                    { 'a' }
                }
            },
            {
                'c',
                new char[,]
                {
                    { ' ', ' ', 'a', ' ', ' ' },
                    { ' ', 'b', ' ', 'b', ' ' },
                    { 'c', ' ', ' ', ' ', 'c' },
                    { ' ', 'b', ' ', 'b', ' ' },
                    { ' ', ' ', 'a', ' ', ' ' }
                }
            }
        };
        upCaseCharToDiamondMap = new Dictionary<char, char[,]>
        {
            {
                'A', 
                new char[,]
                {
                    { 'A' }
                }
            },
            {
                'C',
                new char[,]
                {
                    { ' ', ' ', 'A', ' ', ' ' },
                    { ' ', 'B', ' ', 'B', ' ' },
                    { 'C', ' ', ' ', ' ', 'C' },
                    { ' ', 'B', ' ', 'B', ' ' },
                    { ' ', ' ', 'A', ' ', ' ' }
                }
            }
        };
    }
    
    [Test]
    public void CreateInMemoryDiamond_UseIndexToGetTheLowCaseItem_ValidateTheResult()
    {
        foreach (var mapPair in lowCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            var diamondBase = new TopLeftDiamondPartBuilder().Build(code, baseCode);
            var memoryDiamond = new MemoryOutput(diamondBase);
            new DiamondPrinter(diamondBase, memoryDiamond).Print();
            
            var dimension = mapPair.Value.GetLength(1);
            for (var row = 0; row < dimension; row++) 
            { 
                for (var column = 0; column < dimension; column++)
                {
                    Assert.Multiple(() =>
                    {
                        Assert.That(memoryDiamond[row, column], Is.EqualTo(mapPair.Value[row, column]));
                        Assert.That(memoryDiamond.Diamond[row, column], Is.EqualTo(mapPair.Value[row, column]));
                    });
                }
            }
        }
    }
    
    [Test]
    public void CreateInMemoryDiamond_UseIndexToGetTheUpCaseItem_ValidateTheResult()
    {
        foreach (var mapPair in upCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            var diamondBase = new TopLeftDiamondPartBuilder().Build(code, baseCode);
            var memoryDiamond = new MemoryOutput(diamondBase);
            new DiamondPrinter(diamondBase, memoryDiamond).Print();
            
            var dimension = mapPair.Value.GetLength(1);
            for (var row = 0; row < dimension; row++) 
            { 
                for (var column = 0; column < dimension; column++)
                {
                    Assert.Multiple(() =>
                    {
                        Assert.That(memoryDiamond[row, column], Is.EqualTo(mapPair.Value[row, column]));
                        Assert.That(memoryDiamond.Diamond[row, column], Is.EqualTo(mapPair.Value[row, column]));
                    });
                }
            }
        }
    }
}