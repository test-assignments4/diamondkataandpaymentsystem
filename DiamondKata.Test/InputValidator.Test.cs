using System.Text;
using DiamondKata.Validator;

namespace DiamondKata.Test;

public class InputValidator
{
    private const byte LowerCaseAlphabetBase = 96;
    private const byte UpperCaseAlphabetBase = 64;
    
    [Test]
    public void ProvideASCIILowerCaseAlphabet_GetCodeAndBaseCode_ValidateResult()
    {
        var chIndex = 0;
        while (chIndex < 28)
        {
            var ch = (char)(LowerCaseAlphabetBase + chIndex);
            if (chIndex != 0 && chIndex != 27)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(ch.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
                    Assert.That(baseCode, Is.EqualTo(LowerCaseAlphabetBase));
                    Assert.That(code, Is.EqualTo(chIndex + LowerCaseAlphabetBase));
                });
            }
            else
            {
                Assert.Multiple(() =>
                {
                    Assert.That(ch.IfDiamondKataSymbol(out var code, out var baseCode), Is.False);
                    Assert.That(baseCode, Is.EqualTo(byte.MinValue));
                    Assert.That(code, Is.EqualTo(byte.MinValue));
                });
            }

            chIndex++;
        }
    }
    
    [Test]
    public void ProvideASCIIUpperCaseAlphabet_GetCodeAndBaseCode_ValidateResult()
    {
        var chIndex = 0;
        while (chIndex < 28)
        {
            var ch = (char)(UpperCaseAlphabetBase + chIndex);
            if (chIndex != 0 && chIndex != 27)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(ch.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
                    Assert.That(baseCode, Is.EqualTo(UpperCaseAlphabetBase));
                    Assert.That(code, Is.EqualTo(chIndex + UpperCaseAlphabetBase));
                });
            }
            else
            {
                Assert.Multiple(() =>
                {
                    Assert.That(ch.IfDiamondKataSymbol(out var code, out var baseCode), Is.False);
                    Assert.That(baseCode, Is.EqualTo(byte.MinValue));
                    Assert.That(code, Is.EqualTo(byte.MinValue));
                });
            }

            chIndex++;
        }
    }
}