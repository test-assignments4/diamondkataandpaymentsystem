using DiamondKata.DynamicDiamond.Iterator;
using DiamondKata.Validator;

namespace DiamondKata.Test;

public class DynamicDiamondTest
{
    private Dictionary<char, char[,]> lowCaseCharToDiamondMap;
    private Dictionary<char, char[,]> upCaseCharToDiamondMap;
    
    [SetUp]
    public void Setup()
    {
        lowCaseCharToDiamondMap = new Dictionary<char, char[,]>
        {
            {
                'a', 
                new char[,]
                {
                    { 'a' }
                }
            },
            {
                'c',
                new char[,]
                {
                    { ' ', ' ', 'a', ' ', ' ' },
                    { ' ', 'b', ' ', 'b', ' ' },
                    { 'c', ' ', ' ', ' ', 'c' },
                    { ' ', 'b', ' ', 'b', ' ' },
                    { ' ', ' ', 'a', ' ', ' ' }
                }
            }
        };
        upCaseCharToDiamondMap = new Dictionary<char, char[,]>
        {
            {
                'A', 
                new char[,]
                {
                    { 'A' }
                }
            },
            {
                'C',
                new char[,]
                {
                    { ' ', ' ', 'A', ' ', ' ' },
                    { ' ', 'B', ' ', 'B', ' ' },
                    { 'C', ' ', ' ', ' ', 'C' },
                    { ' ', 'B', ' ', 'B', ' ' },
                    { ' ', ' ', 'A', ' ', ' ' }
                }
            }
        };
    }

    [Test]
    public void CreateDynamicDiamond_UseIndexToGetTheLowCaseItem_ValidateTheResult()
    {
        foreach (var mapPair in lowCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            
            var dynamicDiamond = new DynamicDiamond.DynamicDiamond(code, baseCode);
            
            var dimension = mapPair.Value.GetLength(1);
            for (var row = 0; row < dimension; row++) 
            { 
                for (var column = 0; column < dimension; column++)
                {
                    Assert.That(dynamicDiamond[row, column], Is.EqualTo(mapPair.Value[row, column]));
                }
            }
        }
    }
    
    [Test]
    public void CreateDynamicDiamond_UseIndexToGetTheUpCaseItem_ValidateTheResult()
    {
        foreach (var mapPair in upCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            
            var dynamicDiamond = new DynamicDiamond.DynamicDiamond(code, baseCode);
            
            var dimension = mapPair.Value.GetLength(1);
            for (var row = 0; row < dimension; row++) 
            { 
                for (var column = 0; column < dimension; column++)
                {
                    Assert.That(dynamicDiamond[row, column], Is.EqualTo(mapPair.Value[row, column]));
                }
            }
        }
    }
    
    [Test]
    public void CreateDynamicDiamond_UseEnumeratorToIterateThroughUpCaseItems_ValidateTheResult()
    {
        foreach (var mapPair in upCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            
            var enumerableDiamond = new EnumerableDiamond(code, baseCode);
            foreach (var cell in enumerableDiamond)
            {
                Assert.That(cell, Is.EqualTo(mapPair.Value[enumerableDiamond.Row, enumerableDiamond.Column]));
            }
        }
    }
    
    [Test]
    public void CreateDynamicDiamond_UseEnumeratorToIterateThroughLowCaseItems_ValidateTheResult()
    {
        foreach (var mapPair in lowCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            
            var enumerableDiamond = new EnumerableDiamond(code, baseCode);
            foreach (var cell in enumerableDiamond)
            {
                Assert.That(cell, Is.EqualTo(mapPair.Value[enumerableDiamond.Row, enumerableDiamond.Column]));
            }
        }
    }
    
    [Test]
    public void CreateDynamicDiamond_UseYieldedToIterateThroughLowCaseItems_ValidateTheResult()
    {
        foreach (var mapPair in lowCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            
            var yieldedDiamond = new YieldedDiamond(code, baseCode);
            foreach (var cell in yieldedDiamond.Diamond())
            {
                Assert.That(cell, Is.EqualTo(mapPair.Value[yieldedDiamond.Row, yieldedDiamond.Column]));
            }
        }
    }
    
    [Test]
    public void CreateDynamicDiamond_UseYieldedToIterateThroughUpCaseItems_ValidateTheResult()
    {
        foreach (var mapPair in upCaseCharToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            
            var yieldedDiamond = new YieldedDiamond(code, baseCode);
            foreach (var cell in yieldedDiamond.Diamond())
            {
                Assert.That(cell, Is.EqualTo(mapPair.Value[yieldedDiamond.Row, yieldedDiamond.Column]));
            }
        }
    }
}