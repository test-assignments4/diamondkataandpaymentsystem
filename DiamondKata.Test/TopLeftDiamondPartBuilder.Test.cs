using DiamondKata.Builder;
using DiamondKata.Validator;

namespace DiamondKata.Test;

public class TopLeftDiamondPartBuilderTest
{
    private Dictionary<char, char[,]> charToDiamondMap;
    
    [SetUp]
    public void Setup()
    {
        charToDiamondMap = new Dictionary<char, char[,]>
        {
            {
                'a', 
                new char[,]
                {
                    { 'a' }
                }
            },
            {
                'c',
                new char[,]
                {
                    { ' ', ' ', 'a' },
                    { ' ', 'b', ' ' },
                    { 'c', ' ', ' ' }
                }
            }
        };
    }

    [Test]
    public void ProvidedMapWithExpectedDiamondTopLeftPart_BuildTheTopLeftDiamondPart_ValidatedTheResult()
    {
        foreach (var mapPair in charToDiamondMap)
        {
            Assert.That(mapPair.Key.IfDiamondKataSymbol(out var code, out var baseCode), Is.True);
            var leftDiamondPart = new TopLeftDiamondPartBuilder().Build(code, baseCode);
            
            Assert.Multiple(() =>
            {
                Assert.That(mapPair.Value.GetLength(0), Is.EqualTo(leftDiamondPart.GetLength(0)));
                Assert.That(mapPair.Value.GetLength(1), Is.EqualTo(leftDiamondPart.GetLength(1)));
                Assert.That(leftDiamondPart.GetLength(0), Is.EqualTo(leftDiamondPart.GetLength(1)));
            });
            
            var dimension = leftDiamondPart.GetLength(1);
            for (var row = 0; row < dimension; row++) 
            { 
                for (var column = 0; column < dimension; column++)
                {
                    Assert.That(leftDiamondPart[row, column], Is.EqualTo(mapPair.Value[row, column]));
                }
            }
        }
    }
}